package com.rsarn.auto.detection.tripinterface;

public class TripBroadcastConfig {
	public static final String ACTION_TRIP_INFO = "com.rsarn.auto.trip_info";
	public static final String CATEGORY_TRIP_START = "com.rsarn.auto.trip_start";
	public static final String CATEGORY_TRIP_STOP = "com.rsarn.auto.trip_stop";
	public static final String CATEGORY_TRIP_PARK = "com.rsarn.auto.trip_park";
	public static final String CATEGORY_TRIP_ERROR = "com.rsarn.auto.trip_error";
	public static final String DATA_LATITUDE = "com.rsarn.auto.trip.latitude";
	public static final String DATA_LONGITUDE = "com.rsarn.auto.trip.longitude";
	public static final String DATA_CONFIDENCE = "com.rsarn.auto.trip.confidence";
}
