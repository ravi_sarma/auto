package com.rsarn.auto.detection.services.lbs;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.GatingCriteria;
import com.rsarn.auto.detection.constants.LatLng;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.sampler.SamplerCallback;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.tripinterface.DetectorError;

public class FusedLocationProvider implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
		LocationListener {

	private GoogleApiClient googleAPIClient;
	TripMode tripMode = TripMode.UNKNOWN;

	SamplerCallback samplerBeingCalled;
	LocationRequest locationRequest;

	Location current = null;
	Location start = null;

	double totalDistance = 0;
	double averageSpeed = 0;

	int sampleCount = 0;

	List<Location> locationList = new ArrayList<Location>();

	public List<Location> getLocationList() {
		return locationList;
		// return Collections.unmodifiableList(locationList);
	}

	public FusedLocationProvider(SamplerCallback sampler) {
		samplerBeingCalled = sampler;
	}

	public void resetVariables() {
		tripMode = TripMode.UNKNOWN;
		current = start = null;
		totalDistance = averageSpeed = 0;
		if (locationList != null && locationList.size() > 0)
			locationList.clear();
	}

	public void requestLocation() {

		if (googleAPIClient == null) {
			googleAPIClient = new GoogleApiClient.Builder(ApplicationUtils.ctx).addApi(LocationServices.API).addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this).build();
		}

		locationRequest = LocationRequest.create();
		// Use high accuracy
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		setFrequency(SamplerConstants.GPS_SAMPLING_RATE);
		googleAPIClient.connect();
	}

	public void setFrequency(long freq) {
		locationRequest.setInterval(freq);
	}

	public GatingCriteria windDown() {

		disconnect();

		GatingCriteria criteria = null;

		Location neededLocation = current;

		if (tripMode.equals(TripMode.SPEED_FIND_START)) {
			neededLocation = start;
		}

		if (neededLocation != null)
			criteria = new GatingCriteria(totalDistance, averageSpeed / sampleCount, new LatLng(neededLocation.getLatitude(),
					neededLocation.getLongitude()));

		resetVariables();

		return criteria;
	}

	public void disconnect() {
		if (googleAPIClient != null) {
			if (googleAPIClient.isConnected()) {
				LocationServices.FusedLocationApi.removeLocationUpdates(googleAPIClient, this);
			}
			/*
			 * After disconnect() is called, the client is considered "dead".
			 */
			googleAPIClient.disconnect();
		}
	}

	/*
	 * Called by Location Services when the request to connect the client
	 * finishes successfully. At this point, you can request the current
	 * location or start periodic updates
	 */
	@Override
	public void onConnected(Bundle dataBundle) {
		LocationServices.FusedLocationApi.requestLocationUpdates(googleAPIClient, locationRequest, this);
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		Logc.e(String.format("TLLocationListener::onConnectionSuspended Error= %d", arg0));
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

		Logc.e(String.format("TLLocationListener::onConnectionFailed Error= %d", connectionResult.getErrorCode()));

		ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalError(DetectorError.NOGOOGLEPLAY);
	}

	// Define the callback method that receives location updates
	@Override
	public void onLocationChanged(Location location) {

		if (tripMode.equals(TripMode.FETCH_CONTINUOS)) {
			// TODO: Check accuracy before adding. 20 Metres ??
			locationList.add(location);
			if (locationList.size() == 1) {
				Logc.v("TLLocationListener::onLocationChanged TripMode.FETCH_CONTINUOS in progress");
			}
			return;
		}

		double speedd = location.getSpeed() * 2.2369;

		if (start == null) {
			start = location;
		}

		if (current == null) {
			current = location;
		}

		Logc.v(String.format("TLLocationListener::onLocationChanged speed %.2f", speedd));

		if (tripMode.equals(TripMode.FETCH_ONCE)) {
			Logc.v(String.format("TLLocationListener::onLocationChanged fetch once mode- Accuracy %.2f", location.getAccuracy()));
			Editor editor = ApplicationUtils.ctx.getSharedPreferences("TRIP_DETECTOR_LIBRARY", Context.MODE_PRIVATE).edit();
			editor.putFloat("PARKING_LAT", (float) location.getLatitude());
			editor.putFloat("PARKING_LONG", (float) location.getLongitude());
			editor.commit();
			this.windDown();
			return;
		}

		if (tripMode.equals(TripMode.SPEED_FIND_START)) {
			Logc.v(String.format("TLLocationListener::onLocationChanged speed find mode - Accuracy %.2f", start.getAccuracy()));
			// TODO - stopping the library results in a NP exception here.
			// Defensive check.
			if (ActivityRecogniserService.getInstance() != null) {
				int threshold = ((ActivityRecogniserService.getInstance().getVehicleState() == VehicleState.POTENTIAL_TRIP_STOP) ? SamplerConstants.GPS_INTRIPSTART_SPEED_THRESHOLD_MPH
						: SamplerConstants.GPS_START_SPEED_THRESHOLD_MPH);
				if (speedd >= threshold) {
					samplerBeingCalled.gatingCriteriaResult(new GatingCriteria(0, speedd, new LatLng(start.getLatitude(), start
							.getLongitude())));
					return;
				}
			} else {
				this.windDown();
			}
		}

		if (tripMode.equals(TripMode.SPEED_FIND_STOP)) {
			Logc.v("TLLocationListener::onLocationChanged stop speed find mode");
			if (speedd < SamplerConstants.GPS_START_SPEED_THRESHOLD_MPH) {
				samplerBeingCalled.gatingCriteriaResult(new GatingCriteria(0, speedd, new LatLng(location.getLatitude(), location
						.getLongitude())));
				return;
			}
		}

		if (tripMode.equals(TripMode.SPEED_FIND_WALK)) {
			Logc.v("TLLocationListener::onLocationChanged walk find mode");
			if (speedd < SamplerConstants.GPS_WALK_SPEED_THRESHOLDE_MPH) {
				samplerBeingCalled.gatingCriteriaResult(new GatingCriteria(0, speedd, new LatLng(location.getLatitude(), location
						.getLongitude())));
				return;
			}
		}

		if (tripMode.equals(TripMode.DIST_FIND)) {
			sumSpeed(speedd);
			// distance between previous reported location and current.
			sumDistance(current, location);
			if (totalDistance >= SamplerConstants.GPS_MIN_DISTANCE_METRES) {
				samplerBeingCalled.gatingCriteriaResult(new GatingCriteria(totalDistance, averageSpeed / sampleCount, new LatLng(location
						.getLatitude(), location.getLongitude())));
			}
		}

		current = location;
	}

	private void sumSpeed(double speed) {
		// TO DO: ignore lower speed. This skews the average
		if (speed > 1) {
			sampleCount++;
			averageSpeed += speed;
		}
	}

	private void sumDistance(Location first, Location next) {
		float[] result = new float[1];
		if (first != null && next != null) {
			Location.distanceBetween(first.getLatitude(), first.getLongitude(), next.getLatitude(), next.getLongitude(), result);
			totalDistance += result[0];
		}
	}

	public enum TripMode {
		SPEED_FIND_START(1), SPEED_FIND_STOP(10), SPEED_FIND_WALK(11), DIST_FIND(100), FETCH_ONCE(101), FETCH_CONTINUOS(110), UNKNOWN(111);
		private TripMode(int mode) {
			this.mode = mode;
		}

		private int mode;
	}

	public FusedLocationProvider setTripMode(TripMode tripMode) {
		this.tripMode = tripMode;
		return this;
	}

}
