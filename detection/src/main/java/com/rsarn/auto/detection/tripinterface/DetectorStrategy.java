package com.rsarn.auto.detection.tripinterface;

public enum DetectorStrategy {
	LOW_POWER_(100), DRIVEWISE_(102);

	private int strat;

	public int getStrategy() {
		return strat;
	}

	DetectorStrategy(int str) {
		this.strat = str;
	}

}
