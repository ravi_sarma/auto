package com.rsarn.auto.detection.constants;

import java.io.Serializable;

public class LatLng implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5940569341384011255L;

	public double latitude;
	public double longitude;
	public double altitude;

	public LatLng(double latitude, double longitude) {
		this.latitude = Math.max(-90, Math.min(90, latitude));
		if ((-180 <= longitude) && (longitude < 180)) {
			this.longitude = longitude;
		} else {
			this.longitude = ((360 + (longitude - 180) % 360) % 360 - 180);
		}
	}
}
