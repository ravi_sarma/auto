package com.rsarn.auto.detection.services.lbs;

public class Lokation {
	public Lokation(double latitude, double longitude, double speed, Long timestamp) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.speed = speed;
		this.timestamp = timestamp;
	}
	public double latitude;
	public double longitude;
	public double speed;
	public Long timestamp;
}
