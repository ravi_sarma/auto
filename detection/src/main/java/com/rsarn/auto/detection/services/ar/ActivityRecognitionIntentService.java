package com.rsarn.auto.detection.services.ar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

/**
 * Service that receives ActivityRecognition updates. It receives updates in the
 * background, even if the main Activity is not visible.
 */
public class ActivityRecognitionIntentService extends IntentService {

	public ActivityRecognitionIntentService() {
		// Set the label for the service's background thread
		super("ActivityRecognitionIntentService");
	}

	/**
	 * Called when a new activity detection update is available.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		// Runs on a worker thread and processes if the intent contains an
		// update,
		if (ActivityRecognitionResult.hasResult(intent)) {
			// Get the update
			ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

			// Possiblility of app/service getting killed
			if (ActivityRecogniserService.getInstance() != null) {
				processActivityRecognitionResult(result);
			} else {
				return;
				// this.stopSelf();
			}
		}
	}

	// Calls the Trip identifier logic

	private void processActivityRecognitionResult(ActivityRecognitionResult result) {

		SimpleDateFormat mDateFormat = (SimpleDateFormat) DateFormat.getDateTimeInstance();
		mDateFormat.applyPattern("HH:mm:ss");
		String timeStamp = mDateFormat.format(new Date());

		// Get all the probably activities from the updated result
		ActivityRecogniserService.getInstance().processActivity(result, timeStamp);
	}

	public static String getNameFromType(int activityType) {
		switch (activityType) {
		case DetectedActivity.IN_VEHICLE:
			return "in_vehicle";
		case DetectedActivity.ON_BICYCLE:
			return "on_bicycle";
		case DetectedActivity.ON_FOOT:
			return "on_foot";
		case DetectedActivity.STILL:
			return "still";
		case DetectedActivity.UNKNOWN:
			return "unknown";
		case DetectedActivity.TILTING:
			return "tilting";
		}
		return "unknown";
	}
}
