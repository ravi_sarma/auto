package com.rsarn.auto.detection.sampler;

import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.tripinterface.DetectorError;

import java.util.Date;


//When a Trigger is hit, increase the AR querying frequency.
//When Sampling Complete, decrease the AR frequency to default.
//	==================================================================================================================================
//	Type	|	Trigger			 		|		Sampling
//	==================================================================================================================================
//	Start	|	Bicycle||Vehicle>30		|	180 Sec Vehicle>60 [AR sometimes gives false vehicle confidence in random. Increase sampling confidence and sampling time]
//	Stop	|	Vehicle<40				|	120	Sec Vehicle<30 [Remove bicycle here. Vehicle is enough or Onfoot would identify trip stop]
//	Walking	|	onFoot>30				|	60 Sec	OnFoot>50   [While really walking outside, onFoot confidence is a good measure of walking. On bicycle confidence is half of onfoot]
//	==================================================================================================================================

public abstract class Sampler {

	// The Sampler states are event based. Waits for event to come after
	// sampling interval to
	// take action.

	protected long samplingStart;
	protected int samplingConfidence;
	protected int sampleCount;
	protected boolean samplingComplete;
	protected int interval;
	protected boolean inProgress = false;
	protected int minSample = SamplerConstants.strategyVariable.min_sample_required;
	protected boolean triggerHit = false;
	protected boolean isWildCardEntry = false;

	public void collectSample(int confidence) {
		// Enter only as part of an ongoing sampling
		if (!samplingComplete) {
			try {
				inProgress = true;
				long currentTime = new Date().getTime();
				if (samplingStart == 0L) {
					samplingStart = currentTime;
				}

				// If we get a delayed ActivityRecogniton callback, ignore this
				// samples

				if ((currentTime - samplingStart) >= interval) {
					// Ignore this sample
					Logc.i("Sampler::collectSample() last item in sample");
					samplingComplete = true;
				} else {
					Logc.i("Sampler::collectSample() confidence increased");
					samplingConfidence += confidence;
					sampleCount++;
				}

				if (StartWalkingSampler.class.isInstance(this) && sampleCount == 1) {
					// Record the start location when user is walking.
					Logc.i("Sampler::collectSample() Walking hit. Stop speed loggin.");
					ActivityRecogniserService.getInstance().disconnectLoggingSpeed();
				}

				if (IdleStateSampler.class.isInstance(this) || PotentialStopStateSampler.class.isInstance(this)) {
					if (confidence >= SamplerConstants.strategyVariable.start_wildCard_confidence) {
						isWildCardEntry = true;
						Logc.i("Sampler::collectSample() Start wildcard confidence hit");
						samplerAction(currentTime);
						return;
					}
				}

				if (StartStateSampler.class.isInstance(this)) {
					if (confidence <= SamplerConstants.strategyVariable.stop_wildCard_confidence) {
						isWildCardEntry = true;
						Logc.i("Sampler::collectSample() Stop wildcard confidence hit");
						samplerAction(currentTime);
						return;
					}
				}

				if (StartWalkingSampler.class.isInstance(this)) {
					// GPSStatusChecker.getCurrentLocation();
					if (confidence >= SamplerConstants.strategyVariable.walk_wildCard_confidence) {
						isWildCardEntry = true;
						Logc.i("Sampler::collectSample() Walk wildcard confidence hit");
						samplerAction(currentTime);
						return;
					}
				}

				if (samplingComplete) {
					if (sampleCount >= minSample) {
						samplerAction(currentTime);
					} else {
						resetAll();
					}
				}
			} catch (Exception ex) {
				if (ActivityRecogniserService.getInstance() != null)
					ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalError(DetectorError.UNKNOWN);
				Logc.e(ex.getMessage());
			}
		} else {
			Logc.v("Sampler::collectSample() Ignoring the sample. Data crunching in progress");
		}
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public abstract void samplerAction(long time);

	public abstract void stateChangeAction();

	public void resetAll() {
		inProgress = false;
		samplingStart = 0L;
		samplingConfidence = sampleCount = 0;
		samplingComplete = false;
		triggerHit = false;
		isWildCardEntry = false;
	}

	public boolean isActive() {
		return inProgress;
	}

	public void setMinSample(int minSample) {
		this.minSample = minSample;
	}

	public boolean isTriggerHit() {
		return triggerHit;
	}

	public void setTriggerHit() {
		this.triggerHit = true;
	}
}
