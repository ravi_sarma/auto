package com.rsarn.auto.detection.services.lbs;

import android.Manifest.permission;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;

import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.tripinterface.DetectorError;

public class BatteryLevelChecker {

	BatteryLevelReceiver blr;
	Handler serviceHandler;

	public BatteryLevelChecker(Handler serviceHandler) {
		Logc.i("BatteryIntentService constructor");
		this.serviceHandler = serviceHandler;
		blr = new BatteryLevelReceiver();
		registerReceiver();
	}

	public void registerReceiver() {

		IntentFilter filter = new IntentFilter();
		// filter.addAction(Intent.ACTION_BATTERY_CHANGED);
		filter.addAction(Intent.ACTION_BATTERY_LOW);
		filter.setPriority(900);

		Logc.i("BatteryIntentService registerReceiver()");

		if (ApplicationUtils.ctx != null) {
			ApplicationUtils.ctx.registerReceiver(blr, filter, permission.BATTERY_STATS, serviceHandler);
		}
	}

	public void unRegisterReceiver() {
		try {
			ApplicationUtils.ctx.unregisterReceiver(blr);
		} catch (IllegalArgumentException iae) {
			Logc.e(iae.getMessage());
		}
	}

	public class BatteryLevelReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Logc.v("BatteryLevelReceiver Intent received");
			float level = processIntent(intent);
			if (level <= 25) {
				if (ActivityRecogniserService.getInstance() != null && ActivityRecogniserService.getInstance().isTripInProgress()) {
					// report a stop with last known location
					ActivityRecogniserService.getInstance().tripCompleteAction(true, GPSStatusChecker.getCurrentLocation());
				}
				if (ActivityRecogniserService.getInstance() != null) {
					ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalError(DetectorError.LOWBATTERY);

				}
			}
		}
	}

	private static float processIntent(Intent batteryIntent) {
		float battery = 0;

		int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		// Error checking that probably isn't needed but I added just in
		// case.
		if (level == -1 || scale == -1) {
			battery = 50.0f;
		}

		battery = ((float) level / (float) scale) * 100.0f;

		Logc.i(String.format("Battery Level is %.2f", battery));

		return battery;
	}

	public static boolean getBatteryLevelCriteria() {
		float battery = 0;
		if (ApplicationUtils.ctx != null) {
			Intent batteryIntent = ApplicationUtils.ctx.getApplicationContext().registerReceiver(null,
					new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			battery = processIntent(batteryIntent);
			if (battery <= 25) {
				if (ActivityRecogniserService.getInstance() != null) {
					if (ActivityRecogniserService.getInstance().isTripInProgress()) {
						// report a stop with last known location
						ActivityRecogniserService.getInstance().tripCompleteAction(true, GPSStatusChecker.getCurrentLocation());
					}
					ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalError(DetectorError.LOWBATTERY);
				}
			}

		}
		return (battery > 25 ? true : false);
	}

}
