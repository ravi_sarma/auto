package com.rsarn.auto.detection.tripinterface;

import com.google.android.gms.maps.model.LatLng;

public interface TripCallbacks {

	public void OnTripStart(DetectorConfidence confidence, LatLng coordinates);

	public void OnTripStop(DetectorConfidence confidence, LatLng coordinates);

	public void OnParking(DetectorConfidence confidence, LatLng coordinates);

	public void OnError(DetectorError reason);

}
