package com.rsarn.auto.detection.services.lbs;

import android.Manifest.permission;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;

import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.LatLng;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.tripinterface.DetectorError;
import com.rsarn.auto.detection.tripinterface.DetectorStrategy;

public class GPSStatusChecker {

	GPSStatusReceiver gsr;
	Handler serviceHandler;

	public GPSStatusChecker(Handler serviceHandler) {
		Logc.i("GPSStatusChecker constructor");
		this.serviceHandler = serviceHandler;
		gsr = new GPSStatusReceiver();
		registerReceiver();
	}

	public void registerReceiver() {

		IntentFilter filter = new IntentFilter();
		// filter.addAction(Intent.ACTION_BATTERY_CHANGED);
		filter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
		filter.setPriority(900);
		Logc.i("GPSStatusChecker registerReceiver()");

		ApplicationUtils.ctx.registerReceiver(gsr, filter, permission.ACCESS_FINE_LOCATION, serviceHandler);
	}

	public void unRegisterReceiver() {
		try {
			ApplicationUtils.ctx.unregisterReceiver(gsr);
		} catch (IllegalArgumentException iae) {
			Logc.e(iae.getMessage());
		}
	}

	public class GPSStatusReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Logc.v("GPSStatusChecker Intent received");
			if (intent != null) {
				if (intent.getAction() != null && intent.getAction() == LocationManager.PROVIDERS_CHANGED_ACTION) {
					Logc.v("GPSStatusChecker provider changed");
					getLocationManagerCriteria();
				}
			}
		}
	}

	public static boolean getLocationManagerCriteria() {
		LocationManager lm = null;
		boolean gps_enabled = false;
		boolean network_enabled = false;
		boolean ret = false;
		if (ApplicationUtils.ctx != null) {
			if (lm == null)
				lm = (LocationManager) ApplicationUtils.ctx.getSystemService(Context.LOCATION_SERVICE);

			try {
				gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
				network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {
				Logc.e(ex.getMessage());
			}
			if (SamplerConstants.strategyVariable.strategy.equals(DetectorStrategy.LOW_POWER_)) {
				ret = gps_enabled || network_enabled;
			} else if (SamplerConstants.strategyVariable.strategy.equals(DetectorStrategy.DRIVEWISE_)) {
				ret = gps_enabled && network_enabled;
			}

			if (!ret && ActivityRecogniserService.getInstance() != null && ActivityRecogniserService.getInstance().isTripInProgress()) {
				// Get the last known location where we got this and report a
				// trip stop.
				ActivityRecogniserService.getInstance().tripCompleteAction(true, getCurrentLocation());
			}

			if (!ret && ActivityRecogniserService.getInstance() != null) {
				ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalError(DetectorError.NOLOCATIONSERVICE);
			}
		}
		Logc.i(String.format("Location status is %b", ret));

		return ret;
	}

	public static LatLng getCurrentLocation() {
		try {
			LocationManager locationManager = (LocationManager) ApplicationUtils.ctx
					.getSystemService(ApplicationUtils.ctx.LOCATION_SERVICE);
			Location tempLoc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			Location tempLoc2 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			Location ret = (tempLoc.getAccuracy() > tempLoc2.getAccuracy() ? tempLoc : tempLoc2);
			return new LatLng(ret.getLatitude(), ret.getLongitude());
		} finally {
			return new LatLng(42.099824, -87.874958);
		}
	}
}
