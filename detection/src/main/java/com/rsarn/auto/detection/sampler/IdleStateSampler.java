package com.rsarn.auto.detection.sampler;

import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.GatingCriteria;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.services.lbs.FusedLocationProvider;
import com.rsarn.auto.detection.tripinterface.DetectorConfidence;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class IdleStateSampler extends Sampler implements SamplerCallback {

	private int currentVehicleConfidence = 0;
	private FusedLocationProvider locListener = null;
	ScheduledExecutorService gatingExecutor;
	private boolean gpsInProgress = false;

	public IdleStateSampler() {
		Logc.i(String.format("IdleStateSampler instance created with %d", this.hashCode()));
	}

	private void initLocationListener() {

		Logc.i("IdleStateSampler::startLocationListener()");
		locListener = new FusedLocationProvider(this);
	}

	public void samplerAction(long time) {

		Logc.i("IdleStateSampler::samplerAction()");

		currentVehicleConfidence = (samplingConfidence / sampleCount);

		// ignoring interrupts while gps in progress
		if (gpsInProgress) {
			Logc.i("IdleStateSampler GPS in progress. ignore this sample");
			return;
		}

		processGatingCriteria();
	}

	public void processGatingCriteria() {
		Logc.i("IdleStateSampler::processFirstGatingCriteria()");
		if ((currentVehicleConfidence >= SamplerConstants.strategyVariable.start_vehicleAve_confidence || isWildCardEntry)
				&& ApplicationUtils.statusOfBatteryAndLocation()) {
			// Check speed > 20 MPH
			Logc.i("IdleStateSampler initiaing GPS SPeed fetch");
			if (locListener == null) {
				initLocationListener();
			}
			// Location Listener in speed only mode
			gpsInProgress = true;
			locListener.windDown();
			locListener.setTripMode(FusedLocationProvider.TripMode.SPEED_FIND_START);
			locListener.requestLocation();
			Logc.i("IdleStateSampler GPS SPeed fetch now");
			gatingExecutor = Executors.newSingleThreadScheduledExecutor();
			gatingExecutor.schedule(new GatingCriteriaRunnable(this), SamplerConstants.GPS_SAMPLING_DURATION_FIRST, TimeUnit.MILLISECONDS);

		} else {
			Logc.i("IdleStateSampler processFirstGatingCriteria not enough confidence to start trip");
			resetAll();
		}
	}

	@Override
	public synchronized void gatingCriteriaResult(GatingCriteria criteria) {
		// cancel any pending executor service action
		if (gatingExecutor != null) {
			gatingExecutor.shutdownNow();
		}

		locListener.windDown();

		Logc.i(String.format("IdleStateSampler::firstGatingCriteriaResult speed is %.2f", criteria.getSpeed()));

		if (criteria.getSpeed() >= SamplerConstants.GPS_START_SPEED_THRESHOLD_MPH) {
			Logc.i("IdleStateSampler::firstGatingCriteriaResult speed passed");
			ActivityRecogniserService.getInstance().getTripDetectorUpdater()
					.internalTripStart(DetectorConfidence.POTENTIAL, criteria.getCoordinates());
			stateChangeAction();
		} else {
			Logc.i("IdleStateSampler::firstGatingCriteriaResult speed failed");
		}
		resetAll();
		gpsInProgress = false;
	}

	@Override
	public void stateChangeAction() {
		// TODO Auto-generated method stub
		ActivityRecogniserService.getInstance().setVehicleState(VehicleState.POTENTIAL_TRIP_START);
		VehicleState.POTENTIAL_TRIP_START.nextState(null);
	}

	class GatingCriteriaRunnable implements Runnable {

		IdleStateSampler sampler;

		GatingCriteriaRunnable(IdleStateSampler sampler) {
			this.sampler = sampler;
		}

		@Override
		public void run() {
			// Make sure we did not request a executor shutdown earlier. If so
			// we wont be needing to update the gating callback.
			if (!sampler.gatingExecutor.isShutdown()) {
				if (sampler.locListener != null) {
					GatingCriteria criteria = sampler.locListener.windDown();
					sampler.gatingCriteriaResult(criteria);
				}
			}
		}
	}
}
