package com.rsarn.auto.detection.constants;


//When a Trigger is hit, increase the AR querying frequency.
//When Sampling Complete, decrease the AR frequency to default.
//	==================================================================================================================================
//	Type	|	Trigger			 		|		Sampling
//	==================================================================================================================================
//	Start	|	Bicycle||Vehicle>30/40	|	180 Sec Vehicle>50 [AR sometimes gives false vehicle confidence in random. Increase sampling confidence and sampling time]
//	Stop	|	Vehicle<40				|	120	Sec Vehicle<30 [Remove bicycle here. Vehicle is enough or Onfoot would identify trip stop]
//	Walking	|	onFoot>30				|	60 Sec	OnFoot>50   [While really walking outside, onFoot confidence is a good measure of walking. On bicycle confidence is half of onfoot]
//	==================================================================================================================================

//	==================================================================================================================================
//	Strategy		|	AR Sampling interval		|		GPS
//	==================================================================================================================================
//	LowPower		|	120 Sec (Actual ~140)		|	None
//	High Accuracy	|	30 Sec  (Actual ~40)		|	Parallel GPS Sampling for 120 sec.  Result as a weighted average: 50% for GPS and 50% for AR
//	==================================================================================================================================

import com.rsarn.auto.detection.tripinterface.StrategyConfigurator;

public class SamplerConstants {

	public static final int HALF_MIN = 30;
	public static final int TF_MIN = 45;
	public static final int ONE_MIN = 60;
	public static final int ONE_HALF_MIN = 90;
	public static final int TWO_MIN = 120;
	public static final int TWO_HALF_MIN = 150;
	public static final int TWO_TF_MIN = 165;
	public static final int THREE_MIN = 180;
	public static final int MILLISECONDS_PER_SECOND = 1000;

	// Get speed.
	public static final int GPS_SAMPLING_DURATION_FIRST = ONE_MIN * MILLISECONDS_PER_SECOND;
	// Get distance
	public static final int GPS_SAMPLING_DURATION_SECOND = THREE_MIN * MILLISECONDS_PER_SECOND;

	// 20MPH to 12MPH
	public static final int GPS_START_SPEED_THRESHOLD_MPH = 12;
	// During trip, threshold is lower to handle driving in the parking lot
	// scenarios.
	public static final int GPS_INTRIPSTART_SPEED_THRESHOLD_MPH = 6;
	public static final int GPS_WALK_SPEED_THRESHOLDE_MPH = 5;
	public static final int GPS_SAMPLING_RATE = 2 * MILLISECONDS_PER_SECOND;
	public static final int GPS_MIN_DISTANCE_METRES = 300;

	// public static final int GPS_GATING_CRITERIA = 75;

	public static int BATTERY_CRITERIA = 25;

	public static void initVariables(StrategyConfigurator vars) {
		strategyVariable = vars;
	}

	public static StrategyConfigurator strategyVariable = StrategyConfigurator.defaultHighAccuracyVariables();
}
