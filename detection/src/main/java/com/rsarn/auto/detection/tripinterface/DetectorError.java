package com.rsarn.auto.detection.tripinterface;

public enum DetectorError {
    LOWBATTERY(300, "Battery less than 25%"), NOLOCATIONSERVICE(301, "GPS/Location Services not enabled"), NOGOOGLEPLAY(302,
            "Cannot connect to Google Play Services"), UNKNOWN(399, "Trip Detector Error");

    int errorCode;
    String errorDescription;

    DetectorError(int code, String desc) {
        this.errorCode = code;
        this.errorDescription = desc;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorDesc() {
        return this.errorDescription;
    }
}
