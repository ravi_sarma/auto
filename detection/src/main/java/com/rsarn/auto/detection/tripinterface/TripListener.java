package com.rsarn.auto.detection.tripinterface;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.InternalCallback;
import com.rsarn.auto.detection.constants.LatLng;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.services.ar.ActivityRemover;
import com.rsarn.auto.detection.services.ar.ActivityRequester;
import com.rsarn.auto.detection.services.lbs.BatteryLevelChecker;
import com.rsarn.auto.detection.services.lbs.GPSStatusChecker;

public class TripListener implements InternalCallback {

	TripCallbacks callback;

	Context context;

	boolean registerForBroadcast = false;

	// The activity recognition update request object
	private ActivityRequester mActivityRequester;

	// The activity recognition update removal object
	private ActivityRemover mActivityRemover;

	private boolean runInBackground = false;

	private boolean clientRegistered = false;

	public TripListener(Context ctx, TripCallbacks callback, boolean registerBroadcast, boolean background) {
		this.callback = callback;
		ApplicationUtils.ctx = context = ctx;
		this.runInBackground = background;
		this.registerForBroadcast = registerBroadcast;
		registerCallback();
		// Get detection requester and remover objects
		mActivityRequester = new ActivityRequester(context);
		mActivityRemover = new ActivityRemover(context);
	}

	public void register() {
		if (!clientRegistered) {
			if (ActivityRecogniserService.getInstance() == null) {
				if (!GPSStatusChecker.getLocationManagerCriteria()) {
					// Location not available.
					this.internalError(DetectorError.NOLOCATIONSERVICE);
					return;
				}
				if (!BatteryLevelChecker.getBatteryLevelCriteria()) {
					// Location not available.
					this.internalError(DetectorError.LOWBATTERY);
					return;
				}
				if (runInBackground) {
					ApplicationUtils.SERVICE_MODE = Service.START_STICKY;
				} else {
					ApplicationUtils.SERVICE_MODE = Service.START_NOT_STICKY;
				}
				startService();
				// Register for ActivityRecognition
				startUpdates(ApplicationUtils.DEFAULT_DETECTION_INTERVAL);
				Logc.i("TripDetector::register()");
			}
			clientRegistered = true;
		}
	}

	public void unregister() {
		if (clientRegistered) {
			if (ActivityRecogniserService.getInstance() != null) {
				stopUpdates();
				if (ActivityRecogniserService.getInstance() != null)
					ActivityRecogniserService.getInstance().windDown();
				stopService();
				Logc.i("TripDetector::unregister()");

			}
			clientRegistered = false;
		}
	}

	public void setStrategy(DetectorStrategy strategy, StrategyConfigurator variables) {
		if (strategy.equals(DetectorStrategy.LOW_POWER_)) {
			if (variables == null) {
				// use defaults if not
				SamplerConstants.initVariables(StrategyConfigurator.defaultLowPowerVariables());
			} else {
				SamplerConstants.initVariables(variables);
			}
		} else if (strategy.equals(DetectorStrategy.DRIVEWISE_)) {
			if (variables == null) {
				// use defaults if not
				SamplerConstants.initVariables(StrategyConfigurator.testingHighAccuracyVariables());
			} else {
				SamplerConstants.initVariables(variables);
			}
		}

	}

	public int getTripState() {

		int state = VehicleState.UNKNOWN.ordinal();

		if (clientRegistered) {
			if (ActivityRecogniserService.getInstance() != null) {
				return ActivityRecogniserService.getInstance().getVehicleState().ordinal();
			}
		}

		return state;
	}

	public boolean getTripDetectorPreCondition() {
		boolean tripCondition = false;
		if (GPSStatusChecker.getLocationManagerCriteria() && BatteryLevelChecker.getBatteryLevelCriteria()) {
			tripCondition = true;
		}
		return tripCondition;
	}

	private void registerCallback() {
		// Register for trip updates
		// ActivityRecogniser.getInstance().registerCallback(this);
		ApplicationUtils.internalCB = this;
	}

	private void startService() {
		// start recogniser service
		Intent i = new Intent(context, ActivityRecogniserService.class);
		context.startService(i);
	}

	private void stopService() {
		// stop recogniser service
		Intent i = new Intent(context, ActivityRecogniserService.class);
		context.stopService(i);
	}

	@Override
	public void startUpdates(int interval) {
		// Check for Google Play services
		if (!servicesConnected()) {
			return;
		}
		// Pass the update request to the requester object
		mActivityRequester.requestUpdates(interval);
	}

	private void stopUpdates() {
		// Check for Google Play services
		if (!servicesConnected()) {
			return;
		}

		if (mActivityRequester.getRequestPendingIntent() != null) {
			// Pass the remove request to the remover object
			mActivityRemover.removeUpdates(mActivityRequester.getRequestPendingIntent());
			/*
			 * Cancel the PendingIntent. Even if the removal request fails,
			 * canceling the PendingIntent will stop the updates.
			 */
			mActivityRequester.getRequestPendingIntent().cancel();
			// clear the existing pendingIntent
			mActivityRequester.setRequestPendingIntent(null);
		}
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// Continue
			Logc.i("TripDetector::servicesConnected() Connected to Google Services");
			return true;
			// Google Play services was not available for some reason
		} else {
			internalError(DetectorError.NOGOOGLEPLAY);
			return false;
		}
	}

	@Override
	public void internalTripStart(DetectorConfidence confidence, LatLng coord) {
		Logc.i("TripDetector::tripStart() Trip started");

		if (callback != null) {
			callback.OnTripStart(confidence, new com.google.android.gms.maps.model.LatLng(coord.latitude, coord.longitude));
		}

		if (registerForBroadcast) {
			sendBroadcast(TripBroadcastConfig.CATEGORY_TRIP_START, coord, confidence.getConfidence());
		}
	}

	@Override
	public void internalTripStop(DetectorConfidence confidence, LatLng coord) {
		Logc.i("TripDetector::tripStop() Trip stopped");
		if (clientRegistered) {

			if (callback != null)
				callback.OnTripStop(confidence, new com.google.android.gms.maps.model.LatLng(coord.latitude, coord.longitude));

			if (registerForBroadcast) {
				sendBroadcast(TripBroadcastConfig.CATEGORY_TRIP_STOP, coord, confidence.getConfidence());
			}
		}
	}

	@Override
	public void internalParking(DetectorConfidence confidence, LatLng coord) {
		Logc.i("TripDetector::internalParking() Parking");
		if (clientRegistered) {

			if (callback != null)
				callback.OnParking(confidence, new com.google.android.gms.maps.model.LatLng(coord.latitude, coord.longitude));

			if (registerForBroadcast) {
				sendBroadcast(TripBroadcastConfig.CATEGORY_TRIP_PARK, coord, confidence.getConfidence());
			}
		}
	}

	@Override
	public void internalError(DetectorError reason) {
		Logc.i("TripDetector::internalError()");
		if (callback != null) {
			callback.OnError(reason);
		}
	}

	private void sendBroadcast(String category, LatLng coord, int confidence) {
		Intent intent = new Intent();
		intent.setAction(TripBroadcastConfig.ACTION_TRIP_INFO);
		intent.addCategory(category);
		intent.putExtra(TripBroadcastConfig.DATA_LATITUDE, coord.latitude);
		intent.putExtra(TripBroadcastConfig.DATA_LONGITUDE, coord.longitude);
		intent.putExtra(TripBroadcastConfig.DATA_CONFIDENCE, confidence);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}
}
