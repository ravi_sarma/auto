package com.rsarn.auto.detection.constants;

import android.app.Service;
import android.content.Context;

import com.rsarn.auto.detection.services.lbs.BatteryLevelChecker;
import com.rsarn.auto.detection.services.lbs.GPSStatusChecker;

public class ApplicationUtils {

	public static Context ctx = null;

	public static int SERVICE_MODE = Service.START_NOT_STICKY;

	public static InternalCallback internalCB = null;

	public static final boolean DEBUGMODE = true;

	public static final String APPTAG = "TRIPDETECTOR";

	// Constants used to establish the activity update interval
	private static final int MILLISECONDS_PER_SECOND = 1000;
	
	public static final int MILLISECONDS_TO_NANO = 1000000;

	// Increased from 20 to 10
	private static final int DEFAULT_DETECTION_INTERVAL_SECONDS = 10;

	private static final int HIGHFREQ_DETECTION_INTERVAL_SECONDS = 5;

	public static final int DEFAULT_DETECTION_INTERVAL = MILLISECONDS_PER_SECOND * DEFAULT_DETECTION_INTERVAL_SECONDS;
	public static final int HIGHFREQ_DETECTION_INTERVAL = MILLISECONDS_PER_SECOND * HIGHFREQ_DETECTION_INTERVAL_SECONDS;

	/*
	 * Define a request code to send to Google Play services This code is
	 * returned in Activity.onActivityResult
	 */
	public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	public static final String ACTION_INCREASE_AR_FREQUENCY = "com.example.android.activityrecognition.ACTION_INCREASE_AR_FREQUENCY";

	public static final String ACTION_DEFAULT_AR_FREQUENCY = "com.example.android.activityrecognition.ACTION_DEFAULT_AR_FREQUENCY";

	// public static

	public static final String SERVICE_STOP_ACTION = "com.rsarn.auto.activityRecogniser.stop";

	public static final String ACTION_START_SAMPLER_TIMEOUT = "com.rsarn.auto.sampler.TripStartSamplerHA";
	public static final String ACTION_STOP_SAMPLER_TIMEOUT = "com.rsarn.auto.sampler.IntermediateTripStopSamplerHA";
	public static final String ACTION_WALK_SAMPLER_TIMEOUT = "com.rsarn.auto.sampler.WalkingOrTripStopIdentifierSamplerHA";
	
	public static void increaseARFrequency() {/*
											 * if (internalCB != null) {
											 * internalCB.startUpdates(
											 * HIGHFREQ_DETECTION_INTERVAL); }
											 */
	}

	public static void defaultARFrequency() {/*
											 * if (internalCB != null) {
											 * internalCB.startUpdates(
											 * DEFAULT_DETECTION_INTERVAL); }
											 */
	}

	public static boolean statusOfBatteryAndLocation() {
		return BatteryLevelChecker.getBatteryLevelCriteria() && GPSStatusChecker.getLocationManagerCriteria();
	}

	public static void sendBroadCast() {
		/*
		 * Intent intent = new Intent();
		 * intent.setAction(ActivityUtils.ACTION_INCREASE_AR_FREQUENCY);
		 * intent.addCategory(ActivityUtils.CATEGORY_LOCATION_SERVICES);
		 * LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);
		 */
	}

	// Constants for constructing the log file name
	public static final String LOG_FILE_NAME_PREFIX = "tripdetector";
	public static final String LOG_FILE_NAME_SUFFIX = ".log";
	// Shared Preferences repository name
	public static final String SHARED_PREFERENCES = "com.rsarn.auto.SHARED_PREFERENCES";
	// Keys in the repository for storing the log file info
	public static final String KEY_LOG_FILE_NUMBER = "com.rsarn.auto.KEY_LOG_FILE_NUMBER";
	public static final String KEY_LOG_FILE_NAME = "com.rsarn.auto.KEY_LOG_FILE_NAME";
}
