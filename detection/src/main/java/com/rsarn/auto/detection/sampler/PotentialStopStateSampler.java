package com.rsarn.auto.detection.sampler;

import com.rsarn.auto.detection.constants.GatingCriteria;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.services.lbs.FusedLocationProvider;

import java.util.concurrent.ScheduledExecutorService;


public class PotentialStopStateSampler extends Sampler implements SamplerCallback {

	private int currentVehicleConfidence = 0;
	private FusedLocationProvider locListener = null;
	ScheduledExecutorService gatingExecutor;
	private boolean gpsInProgress = false;

	public PotentialStopStateSampler() {
		Logc.i(String.format("PotentialStopStateSampler instance created with %d", this.hashCode()));
	}

	private void initLocationListener() {

		Logc.i("PotentialStopStateSampler::startLocationListener()");
		locListener = new FusedLocationProvider(this);
	}

	public void samplerAction(long time) {

		Logc.i("PotentialStopStateSampler::samplerAction()");

		currentVehicleConfidence = (samplingConfidence / sampleCount);

		// ignoring interrupts while gps in progress
		if (gpsInProgress) {
			Logc.i("PotentialStopStateSampler GPS in progress. ignore this sample");
			return;
		}

		processGatingCriteria();
	}

	public void processGatingCriteria() {
		Logc.i("PotentialStopStateSampler::processFirstGatingCriteria()");
		// TODO: removed for sharing with others

	}

	@Override
	public synchronized void gatingCriteriaResult(GatingCriteria criteria) {
		// cancel any pending executor service action
		// TODO: removed for sharing with others
	}

	@Override
	public void stateChangeAction() {
		// TODO Auto-generated method stub
		// Remove Timer
		ActivityRecogniserService.getInstance().removeTripEndFinder();
		ActivityRecogniserService.getInstance().setVehicleState(VehicleState.TRIP_START);
		ActivityRecogniserService.getInstance().reStartLoggingSpeed();
	}

	class GatingCriteriaRunnable implements Runnable {

		PotentialStopStateSampler sampler;

		GatingCriteriaRunnable(PotentialStopStateSampler sampler) {
			this.sampler = sampler;
		}

		@Override
		public void run() {
			// Make sure we did not request a executor shutdown earlier. If so
			// we wont be needing to update the gating callback.
			if (!sampler.gatingExecutor.isShutdown()) {
				if (sampler.locListener != null) {
					GatingCriteria criteria = sampler.locListener.windDown();
					sampler.gatingCriteriaResult(criteria);
				}
			}
		}
	}
}
