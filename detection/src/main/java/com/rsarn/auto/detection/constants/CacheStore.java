package com.rsarn.auto.detection.constants;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;

public final class CacheStore {
	private CacheStore() {
	}

	public static void cacheObject(Context context, String key, Object object) throws IOException {
		deleteObject(context,key);
		Logc.i("CacheStore::cacheObject");
		FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(object);
		oos.close();
		fos.close();
	}

	public static Object readObject(Context context, String key) throws IOException, ClassNotFoundException {
		Logc.i("CacheStore::readObject");
		FileInputStream fis = context.openFileInput(key);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Object object = ois.readObject();
		return object;
	}

	public static void deleteObject(Context context, String key) {
		Logc.i("CacheStore::deleteObject");
		context.deleteFile(key);
	}
}
