package com.rsarn.auto.detection.services.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.InternalCallback;
import com.rsarn.auto.detection.constants.LatLng;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.lbs.BatteryLevelChecker;
import com.rsarn.auto.detection.services.lbs.FusedLocationProvider;
import com.rsarn.auto.detection.services.lbs.GPSStatusChecker;
import com.rsarn.auto.detection.services.lbs.Lokation;

public class ActivityRecogniserService extends Service {

    public TripEndFinderRunnable tripEndFinderRunnable;

    boolean endFinderQueued = false;

    public InternalCallback tripDetectorUpdater;

    // Thread class which has a looper
    private HandlerThread looperThread;

    public HandlerForService handler = null;

    Lock lock = new ReentrantLock();

    Lock speedLock = new ReentrantLock();

    Lock stateLock = new ReentrantLock();

    FusedLocationProvider fusedLocProvider;

    LatLng zeroSpeedLocation = null;

    BatteryLevelChecker batteryLevelChecker;

    GPSStatusChecker gpsChecker;

    private VehicleState vehicleState = VehicleState.UNKNOWN;

    // Service by default is a singleton

    private static ActivityRecogniserService instance;

    public static ActivityRecogniserService getInstance() {
        return instance;
    }

    public class HandlerForService extends Handler {
        public HandlerForService(Looper looper) {
            super(looper);
        }
    }

    @Override
    public void onCreate() {

        super.onCreate();

        Logc.i("ActivityRecogniser::onCreate()");

        instance = this;

        looperThread = new HandlerThread("Worker Thread");
        looperThread.start();

        handler = new HandlerForService(looperThread.getLooper());

        batteryLevelChecker = new BatteryLevelChecker(handler);
        gpsChecker = new GPSStatusChecker(handler);

        tripEndFinderRunnable = new TripEndFinderRunnable();

        if (ApplicationUtils.internalCB != null) {
            this.registerCallback(ApplicationUtils.internalCB);
        }

        initVehicleState();

        Logc.i(String.format("ActivityRecogniser Service instance created with %d", instance.hashCode()));
    }

    // Make it a sticky service.
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logc.i("ActivityRecogniser::onStartCommand()");
        return ApplicationUtils.SERVICE_MODE;
    }

    public void processActivity(ActivityRecognitionResult result, String timeStamp) {
        lock.lock();
        for (DetectedActivity detectedActivity : result.getProbableActivities()) {
            // Get the activity type, confidence level
            int activityType = detectedActivity.getType();

            if (activityType == DetectedActivity.ON_BICYCLE || activityType == DetectedActivity.IN_VEHICLE
                    || activityType == DetectedActivity.ON_FOOT) {
                getVehicleState().nextState(detectedActivity);
            } else {
                continue;
            }

            Logc.v(String.format(timeStamp + ": Activity Type:" + ActivityRecognitionIntentService.getNameFromType(activityType)
                    + ", Confidence:%d", detectedActivity.getConfidence()));
        }
        lock.unlock();
    }

    // @Override version for timeout runnable call.
    public void processActivity(DetectedActivity detectedActivity) {
        lock.lock();
        getVehicleState().nextState(detectedActivity);
        lock.unlock();
    }

    public boolean isTripInProgress() {
        return (getVehicleState().ordinal() >= VehicleState.TRIP_START.ordinal() ? true : false);
    }

    public InternalCallback getTripDetectorUpdater() {
        return tripDetectorUpdater;
    }

    public void registerCallback(InternalCallback cb) {
        this.tripDetectorUpdater = cb;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

	/* *************************************************
     * *************************************************
	 * *********ALL TRIP STATE DATA BELOW***************
	 * *************************************************
	 * *************************************************
	 */

    public VehicleState getVehicleState() {
        stateLock.lock();
        VehicleState state = vehicleState;
        stateLock.unlock();
        return state;
    }

    public void setVehicleState(VehicleState vehicleState) {
        stateLock.lock();
        this.vehicleState = vehicleState;
        storeState();
        stateLock.unlock();
    }

    public synchronized void initVehicleState() {
        if (vehicleState == VehicleState.UNKNOWN) {
            retrieveState();
        }
    }

    private void storeState() {
        // Save the list of entries to internal storage
        Editor editor = ApplicationUtils.ctx.getSharedPreferences("TRIP_DETECTOR_LIBRARY", Context.MODE_PRIVATE).edit();
        editor.putInt("TRIP_STATE", vehicleState.ordinal());
        editor.commit();
    }

    private void retrieveState() {
        // Save the list of entries to internal storage
        SharedPreferences sp = ApplicationUtils.ctx.getSharedPreferences("TRIP_DETECTOR_LIBRARY", Context.MODE_PRIVATE);
        int state = sp.getInt("TRIP_STATE", VehicleState.IDLE.ordinal());
        vehicleState = VehicleState.values()[state];
    }

	/* *************************************************
	 * *************************************************
	 * ********ALL TRIP COMPLETE/RESET INFO BELOW*******
	 * *************************************************
	 * *************************************************
	 */

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        Logc.i("ActivityRecogniser::onDestroy()");
        windDown();
        instance = null;
    }

    public void windDown() {
        resetTripInformation();
        looperThread.quit();
        if (batteryLevelChecker != null) {
            batteryLevelChecker.unRegisterReceiver();
        }
        if (gpsChecker != null) {
            gpsChecker.unRegisterReceiver();
        }
    }

    public synchronized void resetTripInformation() {
        Logc.i("ActivityRecogniser::resetTripInformation");
        removeTripEndFinder();
        stopLoggingSpeed();
        if (speedLog != null) {
            speedLog.clear();
        }
        setVehicleState(VehicleState.IDLE);
    }

    public void tripCompleteAction(boolean forced, LatLng location) {
        Logc.i("ActivityRecogniser::TripCompleteAction() trip complete");
        LatLng coordinatesOfParking = null;
        // TODO: removed for sharing with others
    }

	/* *************************************************
	 * *************************************************
	 * *************ALL RUNNABLES BELOW*****************
	 * *************************************************
	 * *************************************************
	 */

    public void queueTripEndFinder() {
        if (endFinderQueued) {
            return;
        }
        handler.postDelayed(tripEndFinderRunnable, SamplerConstants.strategyVariable.stop_identify_timer);
        endFinderQueued = true;
    }

    public void removeTripEndFinder() {
        if (endFinderQueued) {
            handler.removeCallbacks(tripEndFinderRunnable);
            endFinderQueued = false;
        }
    }

    class TripEndFinderRunnable implements Runnable {

        @Override
        public void run() {
            // long currentTime = new Date().getTime();
            Logc.i("ActivityRecogniser::TripEndFinderRunnable run");
            tripCompleteAction(false, null);
            handler.removeCallbacks(this);
        }
    }

    public void queueIdleSamplerTimeoutRunnable() {
        handler.postDelayed(idleSamplerTimeoutRunnable, SamplerConstants.strategyVariable.start_sampling_interval + 100);
    }

    public void dequeueIdleSamplerTimeoutRunnable() {
        handler.removeCallbacks(idleSamplerTimeoutRunnable);
    }

    private Runnable idleSamplerTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            Logc.i("ActivityRecogniser idleSamplerTimeoutRunnable run");
            processActivity(new DetectedActivity(DetectedActivity.IN_VEHICLE, 50));
        }
    };

    public void dequeueStartSamplerTimeoutRunnable() {
        handler.removeCallbacks(startSamplerTimeoutRunnable);
    }

    public void queueStartSamplerTimeoutRunnable() {
        handler.postDelayed(startSamplerTimeoutRunnable, SamplerConstants.strategyVariable.stop_sampling_interval + 100);
    }

    private Runnable startSamplerTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            Logc.i("ActivityRecogniser startSamplerTimeoutRunnable run");
            processActivity(new DetectedActivity(DetectedActivity.IN_VEHICLE, 40));
        }
    };

    public void dequeueStopSamplerTimeoutRunnable() {
        handler.removeCallbacks(stopSamplerTimeoutRunnable);
    }

    public void queueStopSamplerTimeoutRunnable() {
        handler.postDelayed(stopSamplerTimeoutRunnable, SamplerConstants.strategyVariable.start_sampling_interval + 100);
    }

    private Runnable stopSamplerTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            Logc.i("ActivityRecogniser startSamplerTimeoutRunnable run");
            processActivity(new DetectedActivity(DetectedActivity.IN_VEHICLE, 50));
        }
    };

    public void dequeueWalkSamplerTimeoutRunnable() {
        handler.removeCallbacks(tripWalkTimeoutRunnable);
    }

    public void queueWalkSamplerTimeoutRunnable() {
        handler.postDelayed(tripWalkTimeoutRunnable, SamplerConstants.strategyVariable.walk_sampling_interval + 100);
    }

    private Runnable tripWalkTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            Logc.i("ActivityRecogniser tripWalkTimeoutRunnable run");
            processActivity(new DetectedActivity(DetectedActivity.ON_FOOT, 50));
        }
    };

	/* *************************************************
	 * *************************************************
	 * **********ALL SPEED LOGGING BELOW FOR PARKING****************
	 * *************************************************
	 * *************************************************
	 */

    // On Definite trip start
    public void startLoggingSpeed() {
        Logc.i("ActivityRecogniser::startLoggingSpeed");
        fusedLocProvider = new FusedLocationProvider(null).setTripMode(FusedLocationProvider.TripMode.FETCH_CONTINUOS);
        zeroSpeedLocation = null;
        speedLog.clear();
        fusedLocProvider.requestLocation();
    }

    // On Intermediate trip start
    public void reStartLoggingSpeed() {
        if (fusedLocProvider != null) {
            Logc.i("ActivityRecogniser::reStartLoggingSpeed");
            zeroSpeedLocation = null;
            speedLog.clear();
            fusedLocProvider.windDown();
            fusedLocProvider.setTripMode(FusedLocationProvider.TripMode.FETCH_CONTINUOS);
            fusedLocProvider.requestLocation();
        } else {
            startLoggingSpeed();
        }
    }

    public void stopLoggingSpeed() {
        Logc.i("ActivityRecogniser::stopLoggingSpeed");

        if (fusedLocProvider != null) {
            fusedLocProvider.windDown();
        }
        zeroSpeedLocation = null;
    }

    public void disconnectLoggingSpeed() {
        Logc.i("ActivityRecogniser::disconnectLoggingSpeed");

        if (fusedLocProvider != null) {
            fusedLocProvider.disconnect();
        }
    }

    // On Walking Detected.
    public void processLoggedSpeed(boolean parker, boolean stopper, long afterThisTime) {
        Logc.i("ActivityRecogniser::processLoggedSpeed");
        long debugStartTime = new Date().getTime();

        if (fusedLocProvider != null) {
            // Immediately disconnect so that we dont receive further locations.
            fusedLocProvider.disconnect();
            List<Location> locationList = fusedLocProvider.getLocationList();
            if (locationList != null && locationList.size() > 0) {
                Logc.i(String.format("ActivityRecogniser::processLoggedSpeed got Locationlist size %d", locationList.size()));
                speedLock.lock();
                speedLog.clear();
                for (Location loc : locationList) {
                    writeToSpeedLog(loc, parker, stopper, afterThisTime);
                }
                Logc.i(String.format("ActivityRecogniser::processLoggedSpeed updated Speed log with size %d", speedLog.size()));
                if (speedLog.size() > 0) {
                    identifyZeroSpeed();
                }
                speedLock.unlock();
            }
            fusedLocProvider.windDown();
        }

        Logc.i(String.format("ActivityRecogniser::processLoggedSpeed time taken %d seconds", (new Date().getTime() - debugStartTime) / 1000));
    }

    private Map<Float, List<Lokation>> speedLog = new HashMap<Float, List<Lokation>>();

    private void writeToSpeedLog(Location loc, boolean parker, boolean stopper, long afterThisTime) {

        // Only interested in storing speed less than 3 MPH and 20 metres
        // accuracy
        if (loc.getSpeed() >= 3 || loc.getAccuracy() > 10) {
            return;
        }

        if (parker) {
            // Dont accept entries after parking reported.
            // 5 Sec is delay in reporting sensor info
            if ((loc.getTime() > (afterThisTime - 5000))) {
                return;
            }
        }

        if (stopper) {
            if ((loc.getTime() > (afterThisTime + 120000))) {
                return;
            }
        }

        // Store a good precision so we can identify 0.0 vs 0.1 speed.
        Float key = precision(2, loc.getSpeed());
        Lokation lokation = new Lokation(loc.getLatitude(), loc.getLongitude(), loc.getSpeed(), loc.getTime());

        List<Lokation> bucket = speedLog.get(key);
        if (bucket == null) {
            speedLog.put(key, bucket = new ArrayList<Lokation>());
        }

        bucket.add(lokation);
    }

    private Float precision(int decimalPlace, Float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    private void identifyZeroSpeed() {
        // Sort the keys in natural order
        Logc.i("ActivityRecogniser::identifyZeroSpeed");
        SortedSet<Float> keys = new TreeSet<Float>(speedLog.keySet());
        for (Float key : keys) {
            Lokation zeroLocation = getParkedLocation(key);
            if (zeroLocation != null) {
                Logc.i("ActivityRecogniser::identifyZeroSpeed got parked location");
                zeroSpeedLocation = new LatLng(zeroLocation.latitude, zeroLocation.longitude);
                break;
            }
        }
    }

    private Lokation getParkedLocation(float val) {
        // TODO: check accuracy of Location to less than 10-15 metres.
        // TODO: removed for sharing with others
        return null;
    }

    class LocationComparator implements Comparator<Lokation> {
        @Override
        public int compare(Lokation l1, Lokation l2) {
            return l1.timestamp.compareTo(l2.timestamp);

        }
    }
}
