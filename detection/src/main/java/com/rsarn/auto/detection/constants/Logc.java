package com.rsarn.auto.detection.constants;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.util.Log;

public class Logc {

	private static Lock writeLock = new ReentrantLock();

	public static void i(String string) {
		if (ApplicationUtils.DEBUGMODE) {
			Log.i(ApplicationUtils.APPTAG, string);
		}
	}

	public static void e(String string) {
		if (ApplicationUtils.DEBUGMODE) {
			Log.e(ApplicationUtils.APPTAG, string);
			//writeLog(string);
		}
	}

	public static void d(String string) {
		if (ApplicationUtils.DEBUGMODE) {
			Log.d(ApplicationUtils.APPTAG, string);
		}
	}

	public static void v(String string) {
		if (ApplicationUtils.DEBUGMODE) {
			Log.v(ApplicationUtils.APPTAG, string);
		}
	}

	public static void w(String string) {
		if (ApplicationUtils.DEBUGMODE) {
			Log.w(ApplicationUtils.APPTAG, string);
		}
	}

	public static void syso(String string) {
		if (ApplicationUtils.DEBUGMODE) {
			System.out.println(ApplicationUtils.APPTAG + " " + string);
		}
	}

	private static StringBuilder strBuild = new StringBuilder();

	private static void append(String message) {
		strBuild.append("\n");
		strBuild.append(message);
	}

	private static void writeLog(String text) {
		/*
		 * append(text); if (strBuild.length() >= 2048) { writeLock.lock(); if
		 * (strBuild.length() >= 2048) {
		 * Executors.newSingleThreadExecutor().execute(new Runnable() {
		 * 
		 * @Override public void run() { String msg = strBuild.toString();
		 * strBuild.setLength(0);
		 * LogFile.getInstance(ApplicationUtils.ctx).log(msg); }
		 * 
		 * }); } writeLock.unlock(); }
		 */
	}
}
