package com.rsarn.auto.detection.sampler;

import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.GatingCriteria;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.services.lbs.FusedLocationProvider;
import com.rsarn.auto.detection.tripinterface.DetectorConfidence;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PotentialStartStateSampler extends Sampler implements SamplerCallback {

    private FusedLocationProvider locListener = null;
    ScheduledExecutorService gatingExecutor;
    private boolean gpsInProgress = false;

    public PotentialStartStateSampler() {
        Logc.i(String.format("PotentialStartStateSampler instance created with %d", this.hashCode()));
    }

    private void initLocationListener() {

        Logc.i("PotentialStartStateSampler::startLocationListener()");
        locListener = new FusedLocationProvider(this);
    }

    public void samplerAction(long time) {

        Logc.i("PotentialStartStateSampler::samplerAction()");

        // ignoring interrupts while gps in progress
        if (gpsInProgress) {
            Logc.i("PotentialStartStateSampler GPS in progress. ignore this sample");
            return;
        }

        processGatingCriteria();
    }

    public void processGatingCriteria() {
        Logc.i("PotentialStartStateSampler::processFirstGatingCriteria()");

        if (ApplicationUtils.statusOfBatteryAndLocation()) {

            Logc.i("PotentialStartStateSampler initiaing Distance fetch");

            if (locListener == null) {
                initLocationListener();
            }
            // Location Listener in speed only mode
            gpsInProgress = true;
            locListener.windDown();

            locListener.setTripMode(FusedLocationProvider.TripMode.DIST_FIND);
            locListener.requestLocation();

            gatingExecutor = Executors.newSingleThreadScheduledExecutor();
            Logc.i("PotentialStartStateSampler::processGatingCriteria() distance fetch now");
            gatingExecutor.schedule(new GatingCriteriaRunnable(this), SamplerConstants.GPS_SAMPLING_DURATION_SECOND, TimeUnit.MILLISECONDS);

        } else {
            Logc.i("PotentialStartStateSampler processFirstGatingCriteria not enough confidence to start trip");
            ActivityRecogniserService.getInstance().setVehicleState(VehicleState.IDLE);
        }
    }

    @Override
    public synchronized void gatingCriteriaResult(GatingCriteria criteria) {
        Logc.i("PotentialStartStateSampler::secondGatingCriteriaResult");

        locListener.windDown();

        // cancel any pending executor service action
        if (gatingExecutor != null) {
            gatingExecutor.shutdownNow();
        }

        Logc.i(String.format("PotentialStartStateSampler::SpeedFinderRunnable aveSpeed is %.2f", criteria.getSpeed()));
        Logc.i(String.format("PotentialStartStateSampler::SpeedFinderRunnable distance is %.2f", criteria.getDistance()));

        if (criteria.getDistance() >= SamplerConstants.GPS_MIN_DISTANCE_METRES && ApplicationUtils.statusOfBatteryAndLocation()) {
            Logc.i("PotentialStartStateSampler gatingCriteriaResult Trip started");

            ActivityRecogniserService.getInstance().getTripDetectorUpdater()
                    .internalTripStart(DetectorConfidence.DEFINITE, criteria.getCoordinates());
            stateChangeAction();
        } else {
            ActivityRecogniserService.getInstance().setVehicleState(VehicleState.IDLE);
        }

        // Move it down so that First Gating criteria process is not hit
        gpsInProgress = false;
        resetAll();
    }

    @Override
    public void stateChangeAction() {
        // TODO Auto-generated method stub
        ActivityRecogniserService.getInstance().setVehicleState(VehicleState.TRIP_START);
        ActivityRecogniserService.getInstance().startLoggingSpeed();
    }

    class GatingCriteriaRunnable implements Runnable {

        PotentialStartStateSampler sampler;

        GatingCriteriaRunnable(PotentialStartStateSampler sampler) {
            this.sampler = sampler;
        }

        @Override
        public void run() {
            if (!sampler.gatingExecutor.isShutdown()) {
                if (sampler.locListener != null) {
                    GatingCriteria criteria = sampler.locListener.windDown();
                    sampler.gatingCriteriaResult(criteria);
                }
            }
        }
    }

}
