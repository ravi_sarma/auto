package com.rsarn.auto.detection.constants;


import com.rsarn.auto.detection.tripinterface.DetectorConfidence;
import com.rsarn.auto.detection.tripinterface.DetectorError;

public interface InternalCallback {

	public void internalTripStart(DetectorConfidence confidence, LatLng coordinates);

	public void internalTripStop(DetectorConfidence confidence, LatLng coordinates);

	public void internalParking(DetectorConfidence confidence, LatLng coordinates);

	public void internalError(DetectorError reason);
	
	public void startUpdates(int interval);

}
