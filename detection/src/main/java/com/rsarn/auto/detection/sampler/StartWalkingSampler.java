package com.rsarn.auto.detection.sampler;

import com.rsarn.auto.detection.constants.GatingCriteria;
import com.rsarn.auto.detection.constants.LatLng;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.services.lbs.FusedLocationProvider;
import com.rsarn.auto.detection.tripinterface.DetectorConfidence;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class StartWalkingSampler extends Sampler implements SamplerCallback {

	private int currentWalkingConfidence = 0;
	private FusedLocationProvider locListener = null;
	ScheduledExecutorService gatingExecutor;
	private boolean gpsInProgress = false;

	private void initLocationListener() {

		Logc.i("WalkingSampler::startLocationListener()");
		locListener = new FusedLocationProvider(this);
	}

	@Override
	public void samplerAction(long time) {

		Logc.i("WalkingSampler::samplerAction()");

		currentWalkingConfidence = (samplingConfidence / sampleCount);

		// ignoring interrupts while gps in progress
		if (gpsInProgress) {
			Logc.i("WalkingSampler GPS in progress. ignore this sample");
			return;
		}

		processGatingCriteria();
	}

	public void processGatingCriteria() {
		Logc.i(String.format("WalkingSampler::processFirstGatingCriteria() confidence is %d", currentWalkingConfidence));
		if (currentWalkingConfidence > SamplerConstants.strategyVariable.walk_ave_confidence || isWildCardEntry) {

			// Process speed data we have till the moment we identified walking
			// start.
			ActivityRecogniserService.getInstance().processLoggedSpeed(true, false, samplingStart);

			// Check speed < 20 MPH
			Logc.i("WalkingSampler initiated GPS fetch");
			if (locListener == null) {
				initLocationListener();
			}
			// Location Listener in speed only mode
			gpsInProgress = true;
			locListener.windDown();
			locListener.setTripMode(FusedLocationProvider.TripMode.SPEED_FIND_WALK);
			locListener.requestLocation();
			gatingExecutor = Executors.newSingleThreadScheduledExecutor();
			gatingExecutor.schedule(new FirstGatingCriteriaRunnable(this), SamplerConstants.GPS_SAMPLING_DURATION_FIRST,
					TimeUnit.MILLISECONDS);

		} else {
			Logc.i("WalkingSampler processFirstGatingCriteria not enough confidence to stop trip");
			resetAll();
		}
	}

	@Override
	public void gatingCriteriaResult(GatingCriteria criteria) {
		// cancel any pending executor service action
		if (gatingExecutor != null) {
			gatingExecutor.shutdownNow();
		}

		locListener.windDown();

		Logc.i("WalkingSampler::firstGatingCriteriaResult");
		if (criteria.getSpeed() <= SamplerConstants.GPS_WALK_SPEED_THRESHOLDE_MPH) {
			// Don't reporyt consecutive trip stops
			if (!(ActivityRecogniserService.getInstance().getVehicleState() == VehicleState.POTENTIAL_TRIP_STOP)) {
				reportIntermediateStop(criteria.getCoordinates());
			}
			// okay, user is slowing down now.
			reportParking(criteria.getCoordinates());
			stateChangeAction();
		}

		resetAll();
		gpsInProgress = false;
	}

	@Override
	public void stateChangeAction() {
		// TODO Auto-generated method stub
		ActivityRecogniserService.getInstance().setVehicleState(VehicleState.USER_WALKING);
	}
	
	private void reportIntermediateStop(LatLng coord) {
		Logc.i("WalkingSampler::samplerAction() intermediate stop identified");
		ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalTripStop(DetectorConfidence.POTENTIAL, coord);
		// Check for trip end
		ActivityRecogniserService.getInstance().queueTripEndFinder();
	}

	private void reportParking(LatLng coord) {
		Logc.i("WalkingSampler::samplerAction() intermediate Parking identified");
		ActivityRecogniserService.getInstance().getTripDetectorUpdater().internalParking(DetectorConfidence.POTENTIAL, coord);
	}

	class FirstGatingCriteriaRunnable implements Runnable {

		StartWalkingSampler sampler;

		FirstGatingCriteriaRunnable(StartWalkingSampler sampler) {
			this.sampler = sampler;
		}

		@Override
		public void run() {
			if (!sampler.gatingExecutor.isShutdown()) {
				if (sampler.locListener != null) {
					GatingCriteria criteria = sampler.locListener.windDown();
					sampler.gatingCriteriaResult(criteria);
				}
			}
		}
	}
}
