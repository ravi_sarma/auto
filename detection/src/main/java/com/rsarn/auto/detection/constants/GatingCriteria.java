package com.rsarn.auto.detection.constants;


public class GatingCriteria {

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public LatLng getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(LatLng coordinates) {
		this.coordinates = coordinates;
	}

	public GatingCriteria(double distance, double speed, LatLng latlng) {
		super();
		this.distance = distance;
		this.speed = speed;
		this.coordinates = latlng;
	}

	private double distance;
	private double speed;
	private LatLng coordinates;

}
