package com.rsarn.auto.detection.sampler;


import com.rsarn.auto.detection.constants.GatingCriteria;

public interface SamplerCallback {

	public void gatingCriteriaResult(GatingCriteria criteria);
}
