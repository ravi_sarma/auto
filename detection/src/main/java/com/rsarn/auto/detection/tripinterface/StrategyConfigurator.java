package com.rsarn.auto.detection.tripinterface;


import com.rsarn.auto.detection.constants.SamplerConstants;

public class StrategyConfigurator {

	public DetectorStrategy strategy;

	public int start_trigger_confidence;

	public int start_vehicleAve_confidence;

	public int start_wildCard_confidence;

	public int start_sampling_interval;

	public int stop_trigger_confidence;

	public int stop_vehicleAve_confidence;

	public int stop_wildCard_confidence;

	public int stop_sampling_interval;

	public int walk_trigger_confidence;

	public int walk_ave_confidence;

	public int walk_sampling_interval;

	public int walk_wildCard_confidence;

	public int min_sample_required;

	public int stop_identify_timer;

	public void setStartVariables(int trigger, int average, int sampling, int wildCard) {
		this.start_trigger_confidence = trigger;
		this.start_vehicleAve_confidence = average;
		this.start_sampling_interval = sampling;
		this.start_wildCard_confidence = wildCard;
	}

	public void setStopVariables(int trigger, int average, int sampling, int wildCard) {
		this.stop_trigger_confidence = trigger;
		this.stop_vehicleAve_confidence = average;
		this.stop_sampling_interval = sampling;
		this.stop_wildCard_confidence = wildCard;
	}

	public void setWalkVariables(int trigger, int average, int sampling, int wildCard) {
		this.walk_trigger_confidence = trigger;
		this.walk_sampling_interval = sampling;
		this.walk_ave_confidence = average;
		this.walk_wildCard_confidence = wildCard;
	}

	public void setOthers(int minSample, int stopTimer) {
		this.min_sample_required = minSample;
		this.stop_identify_timer = stopTimer;
	}

	public static StrategyConfigurator defaultLowPowerVariables() {
		StrategyConfigurator sv = new StrategyConfigurator();
		sv.strategy = DetectorStrategy.LOW_POWER_;
		sv.setStartVariables(40, 60, SamplerConstants.TWO_MIN * SamplerConstants.MILLISECONDS_PER_SECOND, 75);
		sv.setStopVariables(40, 30, SamplerConstants.TWO_MIN * SamplerConstants.MILLISECONDS_PER_SECOND, 20);
		sv.setWalkVariables(50, 60, SamplerConstants.ONE_HALF_MIN * SamplerConstants.MILLISECONDS_PER_SECOND, 75);
		sv.setOthers(3, 10 * 60 * SamplerConstants.MILLISECONDS_PER_SECOND);
		return sv;
	}

	public static StrategyConfigurator defaultHighAccuracyVariables() {
		StrategyConfigurator sv = new StrategyConfigurator();
		// TODO: removed for sharing with others
		return sv;
	}

	public static StrategyConfigurator testingHighAccuracyVariables() {
		StrategyConfigurator sv = new StrategyConfigurator();
		sv.strategy = DetectorStrategy.DRIVEWISE_;
		sv.setStartVariables(3, 3, SamplerConstants.TF_MIN * SamplerConstants.MILLISECONDS_PER_SECOND, 6);
		sv.setStopVariables(4, 5, SamplerConstants.ONE_HALF_MIN * SamplerConstants.MILLISECONDS_PER_SECOND, 20);
		sv.setWalkVariables(50, 60, SamplerConstants.ONE_MIN * SamplerConstants.MILLISECONDS_PER_SECOND, 75);

		sv.setOthers(1, 3 * 60 * SamplerConstants.MILLISECONDS_PER_SECOND);
		return sv;
	}
}