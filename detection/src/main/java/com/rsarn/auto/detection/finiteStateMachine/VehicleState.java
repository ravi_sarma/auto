package com.rsarn.auto.detection.finiteStateMachine;

import com.google.android.gms.location.DetectedActivity;
import com.rsarn.auto.detection.constants.ApplicationUtils;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.sampler.IdleStateSampler;
import com.rsarn.auto.detection.sampler.PotentialStartStateSampler;
import com.rsarn.auto.detection.sampler.PotentialStopStateSampler;
import com.rsarn.auto.detection.sampler.Sampler;
import com.rsarn.auto.detection.sampler.StartStateSampler;
import com.rsarn.auto.detection.sampler.StartWalkingSampler;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;

public enum VehicleState {

	IDLE {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.i("VehicleState:IDLE");
			if (activity.getType() == DetectedActivity.ON_BICYCLE || activity.getType() == DetectedActivity.IN_VEHICLE) {
				if (!idleStateSampler.isActive()) {

					if (activity.getConfidence() > SamplerConstants.strategyVariable.start_trigger_confidence
							&& ApplicationUtils.statusOfBatteryAndLocation()) {

						Logc.i("ActivityRecogniser::processDrivingActivities() Trip start trigger hit");
						ActivityRecogniserService.getInstance().dequeueIdleSamplerTimeoutRunnable();
						idleStateSampler.resetAll();
						idleStateSampler.setInterval(SamplerConstants.strategyVariable.start_sampling_interval);
						idleStateSampler.setTriggerHit();
						// This would mark the end of a sampling interval
						ActivityRecogniserService.getInstance().queueIdleSamplerTimeoutRunnable();

					}
				}

				if (activity.getType() == DetectedActivity.IN_VEHICLE && idleStateSampler.isTriggerHit()) {
					idleStateSampler.collectSample(activity.getConfidence());
				}
			}
		}
	},
	POTENTIAL_TRIP_START {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.i("VehicleState:POTENTIAL_TRIP_START");
			if (!potentialStartStateSampler.isActive()) {
				Logc.i("ActivityRecogniser::processDrivingActivities() Trip start trigger hit");
				potentialStartStateSampler.resetAll();
				potentialStartStateSampler.setInterval(SamplerConstants.strategyVariable.start_sampling_interval);
				potentialStartStateSampler.setTriggerHit();
				potentialStartStateSampler.samplerAction(0);
			}
		}
	},
	TRIP_START {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.i("VehicleState:TRIP_START");
			if (activity.getType() == DetectedActivity.IN_VEHICLE) {
				if (!startStateSampler.isActive()) {
					if (activity.getConfidence() < SamplerConstants.strategyVariable.stop_trigger_confidence) {
						Logc.i("ActivityRecogniser::processDrivingActivities() Trip stop trigger hit");
						ActivityRecogniserService.getInstance().dequeueStartSamplerTimeoutRunnable();
						startStateSampler.resetAll();
						startStateSampler.setInterval(SamplerConstants.strategyVariable.stop_sampling_interval);
						startStateSampler.setMinSample(SamplerConstants.strategyVariable.min_sample_required);
						startStateSampler.setTriggerHit();
						ActivityRecogniserService.getInstance().queueStartSamplerTimeoutRunnable();
					}
				}

				if (startStateSampler.isTriggerHit()) {
					startStateSampler.collectSample(activity.getConfidence());
				}

			} else if (activity.getType() == DetectedActivity.ON_FOOT) {

				if (!startWalkStateSampler.isActive()) {
					Logc.i("ActivityRecogniser::processWalkingActivites() onfoot trigger hit");
					if (activity.getConfidence() > SamplerConstants.strategyVariable.walk_trigger_confidence) {
						ActivityRecogniserService.getInstance().dequeueWalkSamplerTimeoutRunnable();
						ApplicationUtils.increaseARFrequency();
						startWalkStateSampler.resetAll();
						startWalkStateSampler.setInterval(SamplerConstants.strategyVariable.walk_sampling_interval);
						startWalkStateSampler.setTriggerHit();
						ActivityRecogniserService.getInstance().queueWalkSamplerTimeoutRunnable();
					}

				}
				if (startWalkStateSampler.isTriggerHit())
					startWalkStateSampler.collectSample(activity.getConfidence());
			}
		}
	},
	POTENTIAL_TRIP_STOP {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.i("VehicleState:POTENTIAL_TRIP_STOP");
			// TODO: removed for sharing with others
		}
	},
	USER_WALKING {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.i("VehicleState:USER_WALKING");
			//TODO: removed for sharing wiht others
		}
	},
	TRIP_STOP {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.e("VehicleState:TRIP_STOP");
		}
	},
	UNKNOWN {
		@Override
		public void nextState(DetectedActivity activity) {
			Logc.e("VehicleState:Unknown");
		}
	};

	public abstract void nextState(DetectedActivity activity);

	static {
		idleStateSampler = new IdleStateSampler();
		potentialStartStateSampler = new PotentialStartStateSampler();
		startStateSampler = new StartStateSampler();
		potentialStopStateSampler = new PotentialStopStateSampler();
		walkingStateSampler = new PotentialStopStateSampler();
		startWalkStateSampler = new StartWalkingSampler();
	}

	private static Sampler idleStateSampler;
	private static Sampler potentialStartStateSampler;
	private static Sampler startStateSampler;
	private static Sampler potentialStopStateSampler;
	private static Sampler startWalkStateSampler;
	private static Sampler walkingStateSampler;

}
