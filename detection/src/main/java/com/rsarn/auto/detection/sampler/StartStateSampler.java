package com.rsarn.auto.detection.sampler;

import com.rsarn.auto.detection.constants.GatingCriteria;
import com.rsarn.auto.detection.constants.Logc;
import com.rsarn.auto.detection.constants.SamplerConstants;
import com.rsarn.auto.detection.finiteStateMachine.VehicleState;
import com.rsarn.auto.detection.services.ar.ActivityRecogniserService;
import com.rsarn.auto.detection.services.lbs.FusedLocationProvider;
import com.rsarn.auto.detection.tripinterface.DetectorConfidence;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class StartStateSampler extends Sampler implements SamplerCallback {

	private int currentVehicleConfidence = 0;
	private FusedLocationProvider locListener = null;
	ScheduledExecutorService firstGatingExecutor;
	boolean gpsInProgress = false;

	private void initLocationListener() {

		Logc.i("StartStateSampler::startLocationListener()");
		locListener = new FusedLocationProvider(this);
	}

	@Override
	public void samplerAction(long time) {

		Logc.i("StartStateSampler::samplerAction()");

		currentVehicleConfidence = (samplingConfidence / sampleCount);

		// ignoring interrupts while gps in progress
		if (gpsInProgress) {
			Logc.i("StartStateSampler GPS in progress. ignore this sample");
			return;
		}

		processGatingCriteria();
	}

	public void processGatingCriteria() {
		Logc.i(String.format("StartStateSampler::processFirstGatingCriteria() confidence is %d", currentVehicleConfidence));
		if (currentVehicleConfidence < SamplerConstants.strategyVariable.stop_vehicleAve_confidence || isWildCardEntry) {
			// Check speed < 20 MPH
			Logc.i("StartStateSampler initiated GPS fetch");
			if (locListener == null) {
				initLocationListener();
			}
			gpsInProgress = true;
			// Location Listener in speed only mode
			locListener.windDown();
			locListener.setTripMode(FusedLocationProvider.TripMode.SPEED_FIND_STOP);
			locListener.requestLocation();
			firstGatingExecutor = Executors.newSingleThreadScheduledExecutor();
			firstGatingExecutor.schedule(new GatingCriteriaRunnable(this), SamplerConstants.GPS_SAMPLING_DURATION_FIRST,
					TimeUnit.MILLISECONDS);

		} else {
			Logc.i("StartStateSampler processFirstGatingCriteria not enough confidence to stop trip");
			resetAll();
		}
	}

	@Override
	public void gatingCriteriaResult(GatingCriteria criteria) {
		// cancel any pending executor service action
		if (firstGatingExecutor != null) {
			firstGatingExecutor.shutdownNow();
		}

		locListener.windDown();

		Logc.i("StartStateSampler::GatingCriteriaResult");
		if (criteria.getSpeed() < SamplerConstants.GPS_START_SPEED_THRESHOLD_MPH) {
			// okay, user is slowing down now.
			Logc.i("StartStateSampler::samplerAction() intermediate stop identified");
			ActivityRecogniserService.getInstance().getTripDetectorUpdater()
					.internalTripStop(DetectorConfidence.POTENTIAL, criteria.getCoordinates());
			stateChangeAction();
		}

		resetAll();
		gpsInProgress = false;
	}

	@Override
	public void stateChangeAction() {
		// TODO Auto-generated method stub
		ActivityRecogniserService.getInstance().setVehicleState(VehicleState.POTENTIAL_TRIP_STOP);
		// Timer for trip end
		ActivityRecogniserService.getInstance().queueTripEndFinder();
	}

	class GatingCriteriaRunnable implements Runnable {

		StartStateSampler sampler;

		GatingCriteriaRunnable(StartStateSampler sampler) {
			this.sampler = sampler;
		}

		@Override
		public void run() {
			if (!sampler.firstGatingExecutor.isShutdown()) {
				if (sampler.locListener != null) {
					GatingCriteria criteria = sampler.locListener.windDown();
					sampler.gatingCriteriaResult(criteria);
				}
			}
		}
	}
}
