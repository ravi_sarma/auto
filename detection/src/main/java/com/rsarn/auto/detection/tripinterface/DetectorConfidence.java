package com.rsarn.auto.detection.tripinterface;

public enum DetectorConfidence {
	POTENTIAL(100), DEFINITE(101);

	private int confidence;

	public int getConfidence() {
		return confidence;
	}

	DetectorConfidence(int conf) {
		this.confidence = conf;
	}

}
