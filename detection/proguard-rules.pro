# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/ccethub/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-injars bin\tripstatemachine.jar
#-outjars tripdetector-v1.1.jar

#-libraryjars 'C:\Program Files (x86)\Java\jre7\lib\rt.jar'
#-libraryjars 'C:\Users\rsarn\Desktop\Development\Android\adt-bundle-windows-x86_64-20140702\sdk\platforms\android-19\android.jar'
#-libraryjars 'C:\Users\rsarn\Desktop\Development\Android\adt-bundle-windows-x86_64-20140702\sdk\add-ons\addon-google_apis_x86-google-19\libs\maps.jar'
#-libraryjars 'C:\Users\rsarn\Desktop\Development\Android\adt-bundle-windows-x86_64-20140702\sdk\extras\google\google_play_services\libproject\gpser_r21\google-play-services\libproject\google-play-services_lib\libs\google-play-services.jar'
#-dontoptimize
#-printmapping tripdetector-v1.1.map
#-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
#-renamesourcefileattribute SourceFile
#-dontpreverify
#-verbose
#-dontwarn android.support.**


#-keepclassmembers,allowoptimization enum  * {
#    public static **[] values();
#    public static ** valueOf(java.lang.String);
#}

#-keepclassmembers class * extends java.io.Serializable {
#    static final long serialVersionUID;
#    private static final java.io.ObjectStreamField[] serialPersistentFields;
#    private void writeObject(java.io.ObjectOutputStream);
#    private void readObject(java.io.ObjectInputStream);
#    java.lang.Object writeReplace();
#    java.lang.Object readResolve();
#}

# Keep - Library. Keep all public and protected classes, fields, and methods.
#-keep public class * {
#    public protected <fields>;
#    public protected <methods>;
#}

# Keep names - _class method names. Keep all .class method names. This may be
# useful for libraries that will be obfuscated again with different obfuscators.
#-keepclassmembers,allowshrinking class * {
#    java.lang.Class class$(java.lang.String);
#    java.lang.Class class$(java.lang.String,boolean);
#}
