# README #

Auto - Detection is a automatic trip detection android library which can detect trip starts, trip stops and parking
** Have removed some logic/code for sharing  

### What is this repository for? ###

* Hosts the Library which provides interface for Trip Detection.
* Uses Google Activity Recognition and GPS to provide trip starts and stops.
* Battery Efficient, uses GPS module only when needed
* Runs always in the background with low memory footprint
* Trips are never missed and detects starts/stops/parking spot within 30 seconds.

### How do I get set up? ###

* Compile the project, get the aar
* Add the library as a dependency in your project

compile(name:'libraryname', ext:'aar')


### Contribution guidelines ###

* unit test cases not added

### Who do I talk to? ###

* sarma.m.ravi@gmail.com